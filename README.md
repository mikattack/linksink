# Linksink

A toy web application for managing a small collection of links in a relational database.  The project features/does not feature the following:

- REST API
  + JSON transport
  + Discoverable resources
  + [Problem Detail](https://tools.ietf.org/html/draft-ietf-appsawg-http-problem-03) style errors
  + Service-wide rate limiting (not per user/account/token)
  + No authentication (should be done at the proxying level)
  + Designed for personal use, so does not support highly concurrent writes
- Responsive web interface
  + Simplistic "search" via filtering
  + Create links with just a URL
  + Edit links in-place
  + Not optimized for bulk creation or editing
- Database
  + File-based for easy setup and maintenance
  + Auditing (planned)
- Example deployments via Ansible

Motivations for this project are fairly selfish:

- Desire to control my own data
- Exercise server-side skills
- Exercise client-side skills
- Easier to manage links via API than by hand
- Easier to integrate links via API
- [Pinboard.in](https://pinboard.in/) is cool
