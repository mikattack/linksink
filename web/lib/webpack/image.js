
const webpack = require('webpack');

exports.loader = function(paths) {
  return {
    module: {
      loaders: [
        {
          test:     /.(png|jpc)$/,
          loaders:  ['url'],
          include:  paths
        }
      ]
    }
  };
}
