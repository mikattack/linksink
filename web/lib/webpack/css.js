
const webpack = require('webpack');

exports.loader = function(paths) {
  return {
    module: {
      loaders: [
        {
          test:     /.css$/,
          loaders:  ['style', 'css', 'sass'],
          include:  paths
        }
      ]
    }
  };
}
