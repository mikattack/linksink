
const merge = require('webpack-merge');
const path = require('path');
const pkg = require('./package.json')
const validate = require('webpack-validator');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const wp = {  // Webpack Parts
  clean:      require('./lib/webpack/clean').clean,
  css:        require('./lib/webpack/css'),
  devserver:  require('./lib/webpack/devserver'),
  extract:    require('./lib/webpack/extract'),
  freevar:    require('./lib/webpack/freevar').setFreeVariable,
  image:      require('./lib/webpack/image'),
  minify:     require('./lib/webpack/minify').minify,
}


const PATHS = {
  src:    path.join(__dirname, 'src'),
  build:  path.join(__dirname, 'build'),
  style:  path.join(__dirname, 'src', 'css', 'import.css')
};
const common = {
  entry: {
    app:      path.join(PATHS.src, 'app.js'),
    style:    PATHS.style,
    vendor:   Object.keys(pkg.dependencies)
  },
  output: {
    path:     PATHS.build,
    filename: '[name].js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Linksink'
    })
  ]
};


var config;
switch (process.env.npm_lifecycle_event) {
  case 'build':
    config = merge(
      common,
      wp.extract.extractCSS(PATHS.style),
      wp.image.loader(PATHS.src),
      wp.minify(),
      wp.freevar('process.evn.NODE_ENV', 'production'),
      wp.extract.extractBundle({
        name:     'vendor',
        entries:  ['react']
      }),
      {
        devtool: 'source-map',
        output: {
          path:        PATHS.build,
          filename:     '[name].[chunkhash].js',
          chunkFilename:'[chunkhash].js'
        }
      },
      wp.clean(path.join(PATHS.build, '*'))
    );
    break;
  default:
    config = merge(
      common,
      wp.css.loader(PATHS.style),
      wp.image.loader(PATHS.src),
      wp.devserver.create({
        host: process.env.HOST,
        port: process.env.PORT
      }),
      { devtool: 'eval-source-map' }
    );
}


module.exports = validate(config);
