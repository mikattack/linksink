package main

import (
  "encoding/json"
  "fmt"
  "io"
  "net/http"
  "net/url"
  "strconv"
  "strings"
  "time"

  "golang.org/x/net/html"
  "github.com/jmoiron/sqlx"
  "gitlab.com/mikattack/linksink/service/middleware"
  "gitlab.com/mikattack/linksink/service/models"
)


func linkCollectionHandler(w http.ResponseWriter, r *http.Request) {
  first := true
  pc := models.PagingConfiguration{}

  // Parse query options
  qs, err := url.ParseQuery(r.URL.RawQuery)
  if err != nil {
    elog.Printf(err.Error())
    http.Error(w, "", http.StatusBadRequest)
    return
  }

  // Determine any paging options
  if v := qs.Get("limit"); len(v) > 0 {
    if parsed, err := strconv.Atoi(v); err == nil {
      pc.Limit = parsed
    }
  }
  if v := qs.Get("offset"); len(v) > 0 {
    if parsed, err := strconv.Atoi(v); err == nil {
      pc.Offset = parsed
    }
  }

  // Select dataset
  var rows *sqlx.Rows
  if term := qs.Get("search"); len(term) > 0 {
    rows, err = models.SearchLinks(term, pc)
  } else {
    rows, err = models.GetLinks(pc)
  }
  if err != nil {
    elog.Print(err.Error())
    http.Error(w, "", http.StatusInternalServerError)
    return
  }

  // Display results
  w.Write([]byte("[\n"))
  for rows.Next() {
    var link models.Link
    err := rows.StructScan(&link)
    if err != nil {
      elog.Printf(err.Error())
      continue
    }

    raw := map[string]string{}
    raw["label"] = link.Label
    raw["href"] = link.Href
    raw["ref"] = fmt.Sprintf("%s/%d", getRequestURI(r), link.Id)

    encoded, err := json.Marshal(raw)
    if err != nil {
      elog.Printf(err.Error())
      continue
    }
    
    if first == false {
      w.Write([]byte(","))
    }

    w.Write(encoded)
    w.Write([]byte("\n"))
    first = false
  }

  w.Write([]byte("]"))
  rows.Close()
}


func linkCreateHandler(w http.ResponseWriter, r *http.Request) {
  input, ok := r.Context().Value("input").(middleware.LinkRequest)
  if ok == false || input.IsEmpty() == true {
    http.Error(w, "", http.StatusBadRequest)
    return
  }

  link := models.Link{
    Label:  input.Label,
    Href:   input.Href,
  }

  // Attempt to fetch the link's "title" element as its label
  if len(link.Label) == 0 {
    config := GetConfig()
    lt := 3
    if parsed, err := strconv.Atoi(config["lookup-timeout"]); err != nil {
      elog.Printf("Cannot parse timeout '%s'", config["lookup-timeout"])
    } else {
      lt = parsed
    }

    url := link.Href
    if index := strings.Index(url, "://"); index > -1 {
      url = url[index + 3:]
    }

    timeout := time.Duration(lt) * time.Second
    client := http.Client{ Timeout:timeout }
    resp, err := client.Get(url)
    if err != nil {
      elog.Print(err.Error())
    } else {
      defer resp.Body.Close()
      if title, ok := getHtmlTitle(resp.Body); ok {
        link.Label = title
      }
    }
  }

  err := link.Save()
  if err != nil {
    elog.Print(err.Error())
    http.Error(w, "", http.StatusInternalServerError)
  } else {
    w.Header().Add("Location", fmt.Sprintf("%s/%d", getRequestURI(r), link.Id))
    w.WriteHeader(http.StatusCreated)
  }

  return
}


func linkFetchHandler(w http.ResponseWriter, r *http.Request) {
  ctx := r.Context()
  link, ok := ctx.Value("link").(models.Link)
  if ok == false {
    elog.Print("Failed to read link information from context")
    http.Error(w, "", http.StatusUnprocessableEntity)
    return
  }

  encoded, err := json.Marshal(link)
  if err != nil {
    elog.Printf(err.Error())
    http.Error(w, "", http.StatusInternalServerError)
    return
  }

  w.Header().Add("Content-Type", "application/json")
  w.Write(encoded)
  return
}


func linkUpdateHandler(w http.ResponseWriter, r *http.Request) {
  input, ok := r.Context().Value("input").(middleware.LinkRequest)
  if ok == false || input.IsEmpty() == true {
    http.Error(w, "", http.StatusBadRequest)
    return
  }

  ctx := r.Context()
  link, ok := ctx.Value("link").(models.Link)
  if ok == false {
    elog.Print("Failed to read link information from context")
    http.Error(w, "", http.StatusUnprocessableEntity)
    return
  }

  // TODO: Add smarter updating semantics
  if len(input.Label) > 0 {
    link.Label = input.Label
  }
  if len(input.Href) > 0 {
    link.Href = input.Href
  }

  err := link.Save()
  if err != nil {
    elog.Print(err.Error())
    http.Error(w, "", http.StatusInternalServerError)
  } else {
    w.WriteHeader(http.StatusOK)
  }

  return
}


func linkDeleteHandler(w http.ResponseWriter, r *http.Request) {
  ctx := r.Context()
  link, ok := ctx.Value("link").(models.Link)
  if ok == false {
    elog.Print("Failed to read link information from context")
    http.Error(w, "", http.StatusUnprocessableEntity)
    return
  }

  err := link.Delete()
  if err != nil {
    elog.Print(err.Error())
    http.Error(w, "", http.StatusInternalServerError)
    return
  }

  w.WriteHeader(http.StatusOK)
  return
}


func getRequestURI(r *http.Request) string {
  protocol := "http://"
  if r.TLS != nil {
    protocol = "https://"
  }
  return fmt.Sprintf("%s%s%s", protocol, r.Host, r.URL.RawPath)
}


func getHtmlTitle(r io.Reader) (string, bool) {
  doc, err := html.Parse(r)
  if err != nil {
    return "", false
  }
  return traverse(doc)
}


func traverse(n *html.Node) (string, bool) {
  if n.Type == html.ElementNode && n.Data == "title" {
    return n.FirstChild.Data, true
  }

  for c := n.FirstChild; c != nil; c = c.NextSibling {
    result, ok := traverse(c)
    if ok {
      return result, ok
    }
  }
  return "", false
}
