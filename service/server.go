package main

import (
  //"context"
  "log"
  "net/http"
  "os"
  "time"
  "strconv"

  "github.com/pressly/chi"
  "github.com/pressly/chi/middleware"

  "gitlab.com/mikattack/linksink/service/database"
  mw "gitlab.com/mikattack/linksink/service/middleware"
)


var elog *log.Logger


func main() {
  // If help was requested, halt program
  for _, arg := range os.Args[1:] {
    if arg == "--help" || arg == "-h" {
      os.Exit(0)
    }
  }

  elog = log.New(os.Stderr, "", log.LstdFlags)

  // Environment/CLI configuration
  config := GetConfig()
  _, err := strconv.Atoi(config["port"])
  if err != nil {
    elog.Fatal("Invalid port: " + config["port"])
  }

  // Switch default logger to output to STDOUT
  log.SetOutput(os.Stdout)

  // Initialize database pool
  conn, err := database.InitializeDatabasePool(config["db"])
  if err != nil {
    elog.Fatal("Initialize database: " + err.Error())
  }
  defer conn.Close()

  // Initialize HTTP server
  mux := chi.NewRouter()

  mux.Use(middleware.Logger)
  mux.Use(middleware.CloseNotify)
  mux.Use(middleware.Timeout(10 * time.Second))
  mux.Use(mw.ParseInput(elog))

  mux.Route("/", func(m chi.Router) {
    m.Get("/", linkCollectionHandler)
    m.Post("/", linkCreateHandler)
    m.Route("/:id", func(m chi.Router) {
      m.Use(mw.LinkCtx(elog))
      m.Get("/", linkFetchHandler)
      m.Put("/", linkUpdateHandler)
      m.Delete("/", linkDeleteHandler)
    })
  })

  elog.Fatal(http.ListenAndServe(":" + config["port"], mux))
}
