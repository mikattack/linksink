package middleware

import (
  "context"
  "log"
  "net/http"
  "strconv"

  "github.com/pressly/chi"
  "gitlab.com/mikattack/linksink/service/models"
)


func LinkCtx(logger *log.Logger) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
	  fn := func(w http.ResponseWriter, r *http.Request) {
	    raw := chi.URLParam(r, "id")
	    linkId, err := strconv.ParseInt(raw, 10, 64)
	    if err != nil {
	      logger.Printf("Failed to parse linkId '%s'", raw)
	      http.Error(w, "", http.StatusInternalServerError)
	      return
	    }

	    link, err := models.GetLink(linkId)
	    if err != nil {
	      logger.Print(err)
	      http.Error(w, "", http.StatusInternalServerError)
	      return
	    }
	    if link.Id == 0 {
	      http.Error(w, "", http.StatusNotFound)
	      return 
	    }

	    ctx := context.WithValue(r.Context(), "linkId", linkId)
	    ctx = context.WithValue(ctx, "link", link)
	    next.ServeHTTP(w, r.WithContext(ctx))
	  }
	  return http.HandlerFunc(fn)
	}
}
