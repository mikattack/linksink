package middleware

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strings"
)


type LinkRequest struct {
	Label 		string  	`json:"label,omitempty"`
	Href  		string  	`json:"href,omitempty"`
	isEmpty		bool
}

func (lr *LinkRequest) IsEmpty() bool {
	return lr.isEmpty
}


func ParseInput(logger *log.Logger) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			var err error
			lq := LinkRequest{}
			
			// Parse JSON input (when possible)
			if r.Method != "POST" && r.Method != "PUT" {
				lq.isEmpty = true	
			} else {
				lq, err = parseLinkRequest(r)
				if err != nil {
					if err != io.EOF {
						logger.Printf(err.Error())
					}
					http.Error(w, "", http.StatusBadRequest)
					return
				}
			}

			// Validate input data
			lq.Label = strings.TrimSpace(lq.Label)
			lq.Href = strings.TrimSpace(lq.Href)
			if len(lq.Label) > 0 || len(lq.Href) > 0 {
				lq.isEmpty = false
			}

			// Add input to context
			ctx := context.WithValue(r.Context(), "input", lq)
			next.ServeHTTP(w, r.WithContext(ctx))
		}
		return http.HandlerFunc(fn)
	}
}


func parseLinkRequest(r *http.Request) (LinkRequest, error) {
	var parsed LinkRequest
	if err := json.NewDecoder(r.Body).Decode(&parsed); err != nil {
		return LinkRequest{}, err
	}
	return parsed, nil
}
