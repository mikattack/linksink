package main

import (
  "os"
  "strconv"
  "github.com/spf13/cobra"
  "github.com/spf13/viper"
)


type option struct {
  Name        string
  EnvName     string
  Default     string
  Description string
  IsBoolean   bool
}


var config map[string]string = map[string]string{ }
var flagmap []option = []option{
  option{
    Name:        "db",
    EnvName:     "DB",
    Default:     "/var/tmp/linksink.db",
    Description: "Path where to store/read SQLite database",
  },
  option{
    Name:        "port",
    EnvName:     "PORT",
    Default:     "48220",
    Description: "Port to host service on",
  },
  option{
    Name:        "lookup-timeout",
    EnvName:     "LOOKUP_TIMEOUT",
    Default:     "3",
    Description: "Timeout duration for link lookups",
  },
}


func parseConfig(argv []string) {
  config = map[string]string{ }   // Reset parsed values

  // There are no sub-commands, so we only use a single command
  cmd := &cobra.Command{
    Use:    "linksink [flags]",
    Short:  "API for managing a collection of hyperlinks",
    Run:    func (cmd *cobra.Command, args []string) {
      // Nothing for now...
    },
  }

  // Register all flags
  for _, opt := range flagmap {
    if opt.IsBoolean {
      parsed, err := strconv.ParseBool(opt.Default)
      if err != nil {
        parsed = false
      }
      cmd.Flags().Bool(opt.Name, parsed, opt.Description)
      viper.SetDefault(opt.Name, parsed)
    } else {
      cmd.Flags().String(opt.Name, opt.Default, opt.Description)
      viper.SetDefault(opt.Name, opt.Default)
    }
    viper.BindEnv(opt.Name, opt.EnvName)
    viper.BindPFlag(opt.Name, cmd.Flags().Lookup(opt.Name))
  }
  viper.AutomaticEnv()

  // Trigger CLI flag parsing
  cmd.SetArgs(argv)
  cmd.Execute()

  // Assign values to internal configuration map
  for _, opt := range flagmap {
    if opt.IsBoolean {
      config[opt.Name] = strconv.FormatBool(viper.GetBool(opt.Name))
    } else {
      config[opt.Name] = viper.GetString(opt.Name)
    }
  }
}


func GetConfig() map[string]string {
  return config
}


func init() {
  parseConfig(os.Args[1:])
}
