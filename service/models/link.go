package models

import (
	"fmt"
	
	"github.com/jmoiron/sqlx"
	db "gitlab.com/mikattack/linksink/service/database"
)


type Link struct {
	Id      		int64     `json:"-"`
	Label   		string    `json:"label"`
	Href    		string    `json:"href"`
	Created 		string		`json:"created"`
	persisted		bool
}

type PagingConfiguration struct {
	Limit		int
	Offset	int
}


func (l *Link) Save() error {
	if l.persisted == true {
		// Attempt record update
		return db.Transaction(func (tx *sqlx.Tx) error {
			// Update table
			sql := "UPDATE links SET label = ?, href = ? WHERE id = ?"
			if stmt, err := tx.Prepare(sql); err != nil {
				return err
			} else if _, err := stmt.Exec(l.Label, l.Href, l.Id); err != nil {
				return err
			}

			// Update FTS table
			sql = "UPDATE ftslinks SET label = ?, href = ? WHERE linkid = ?"
			if stmt, err := tx.Prepare(sql); err != nil {
				return err
			} else if _, err := stmt.Exec(l.Label, l.Href, l.Id); err != nil {
				return err
			}

			return nil
		})
	} else {
		// Attempt record insert
		return db.Transaction(func (tx *sqlx.Tx) error {
			// Insert into table
			sql := "INSERT INTO links (label, href) VALUES (?, ?)"
			if stmt, err := tx.Prepare(sql); err != nil {
				return err
			} else if _, err := stmt.Exec(l.Label, l.Href); err != nil {
				return err
			}
			
			// Fetch new Link identifier
			var lastid int64
			sql = "SELECT last_insert_rowid() FROM links"
			if err := tx.QueryRowx(sql).Scan(&lastid); err != nil {
				return err
			} else {
				l.Id = lastid
				l.persisted = true
			}

			// Insert into FTS table
			sql = "INSERT INTO ftslinks (linkid, label, href) VALUES (?, ?, ?)"
			if stmt, err := tx.Prepare(sql); err != nil {
				return err
			} else if _, err := stmt.Exec(l.Id, l.Label, l.Href); err != nil {
				return err
			}

			return nil
		})
	}
}


func (l *Link) Delete() error {
	return db.Transaction(func (tx *sqlx.Tx) error {
		// Remove from table
		sql := "DELETE FROM links WHERE id = ?"
		if stmt, err := tx.Prepare(sql); err != nil {
			return err
		} else if _, err := stmt.Exec(l.Id); err != nil {
			return err
		}

		// Remove from FTS table
		sql = "DELETE FROM links WHERE linkid = ?"
		if stmt, err := tx.Prepare(sql); err != nil {
			return err
		} else if _, err := stmt.Exec(l.Id); err != nil {
			return err
		}

		return nil
	})
}


func GetLink(id int64) (Link, error) {
	conn, err := db.GetConnection()
	if err != nil {
		return Link{}, err
	}

	var link Link
	err = conn.QueryRowx("SELECT * FROM links WHERE id=?", id).StructScan(&link)
	if link.Id == 0 {
		return link, nil
	}
	if err != nil {
		link.persisted = true
	}
	return link, err
}


func GetLinks(p PagingConfiguration) (*sqlx.Rows, error) {
	limit := db.DEFAULT_LIMIT
	if p.Limit > 0 {
		limit = p.Limit
	}

	conn, err := db.GetConnection()
	if err != nil {
		return &sqlx.Rows{}, err
	}

	// This is a bit sloppy.  It's up to clients to either iterate through
	// all results or close the cursor.  If they do not, dangling connection.
	sql := fmt.Sprintf("SELECT * FROM links ORDER BY created LIMIT %d OFFSET %d", limit, p.Offset)
	return conn.Queryx(sql)
}


func SearchLinks(term string, p PagingConfiguration) (*sqlx.Rows, error) {
	limit := db.DEFAULT_LIMIT
	if p.Limit > 0 {
		limit = p.Limit
	}

	offset := 0
	if p.Offset > 0 {
		offset = p.Offset
	}

	conn, err := db.GetConnection()
	if err != nil {
		return &sqlx.Rows{}, err
	}

	// We actually want full-text search here...someday
	query := `
		SELECT *
		FROM links
		WHERE
			label LIKE ? OR
			href LIKE ?
		ORDER BY created
		LIMIT %d OFFSET %d
	`
	sql := fmt.Sprintf(query, limit, offset)
	term = fmt.Sprintf("%%%s%%", term)
	return conn.Queryx(sql, term, term)
}
