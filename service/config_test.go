package main

import (
  "os"
  "testing"
)


var envs []string = []string{ "DB","PORT","LOOKUP_TIMEOUT" }
var keys []string = []string{ "db","port","lookup-timeout" }
var dflt []string = []string{ "/var/tmp/linksink.db","48220","3" }


func resetEnv() {
	for index, env := range envs {
		os.Setenv(env, dflt[index])
	}
}


// Ensure default options are what is expected
func TestConfigDefaults(t *testing.T) {
	resetEnv()

	parseConfig([]string{})
	config := GetConfig()

	expected := map[string]string{
    "db": "/var/tmp/linksink.db",
    "port": "48220",
    "lookup-timeout": "3",
  }

  for key, value := range expected {
    if (config[key] != value) {
      t.Errorf("[%s] expected to be [%s], found [%s]", key, value, config[key])
    }
  }
}


// Test that environmental variables successfully override defaults
func TestConfigEnv(t *testing.T) {
  resetEnv()

  for _, env := range envs {
    os.Setenv(env, "8")
  }

  parseConfig([]string{})
	config := GetConfig()

  for _, key := range keys {
    if (config[key] != "8") {
      t.Errorf("[%s] expected to be [8], found [%s]", key, config[key])
    }
  }
}


  // Test that commandline flags successfully override defaults
func TestConfigCLI(t *testing.T) {
  resetEnv()

  cli := []string{}
  for _, key := range keys {
    cli = append(cli, "--" + key + "=2")
  }

  parseConfig(cli)
	config := GetConfig()

  for _, key := range keys {
    if (config[key] != "2") {
      t.Errorf("[%s] expected to be [2], found [%s]", key, config[key])
    }
  }
}


// Test that commandline flags successfully override environmental ones
func TestConfigOverride(t *testing.T) {
	resetEnv()

  for _, env := range envs {
    os.Setenv(env, "8")
  }

  cli := []string{}
  for _, key := range keys {
    cli = append(cli, "--" + key + "=2")
  }

  parseConfig(cli)
	config := GetConfig()

	for _, key := range keys {
    if (config[key] != "2") {
      t.Errorf("[%s] expected to be [2], found [%s]", key, config[key])
    }
  }
}
